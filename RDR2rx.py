#!/usr/bin/env python3
"""
Helper to clean out sga vulkan files and manage processes.

 Example:
   python RDR2rx.py -rm-sga -wait [?minutes?] -path [absolute path]

    -path       Expects an absolute path to your steams compdata folder.
                $USER must have ownership of path.
                    Example : -path /media/games/steamapps/compatdata/
                  ** Note *nix only flag. On Windows will be ignored.

    -rm-sga     Will remove the sga_vulkan_final_init files. When used
                with -wait, will delay removal if RDR2 Launcher has
                started recently.
                    Example: $ python RDR2rx.py -rm-sga

       -wait    Must be paired with -rm-sga. Will wait (x) minutes.
                Defaults to 10 minutes.
                    Example: $ python RDR2rx.py -rm-sga -wait
                             $ python RDR2rx.py -rm-sga -wait 5

    -kill
                Will kill a RDR2 process. Must be paired with at least
                one of the following.
                     Example: $ python RDR2rx.py -kill -Launcher

       -Launcher     Will kill Launcher.exe

       -PlayRDR2     Will kill PlayRDR2.exe

       -RDR2         Will kill RDR2.exe

       -all          !Will kill every process ending in .exe!
                       *nix only. Useful as keyboard shortcut for
                       unresponsive proton instance.

Set script to run at intervals using crontab:

  $ chmod +x RDR2rx.py

  $ crontab -e

  * /30 * * * * /path/to/RDR2rx.py -rm-sga -wait -path [path]"""
from os.path import (exists, splitext, abspath, expanduser, dirname,
                     join as oJoin)
from os import listdir, remove, name as oName
from sys import argv, exit as sExit
from datetime import datetime, timedelta
from time import sleep
from psutil import process_iter as psIter

class RedDeadSGA():
    """
Class that handles the removal of sga_vulkan files: """
    def __init__(self, *arg):
        """
Method sets up our absolute path to our RDR2 directory. The default is
relative to this files location. Can be overridden with the -path flag.
"""
        self.base_dir = abspath(dirname(__file__))
        self.rdr2_loc = self.build_path(arg)

    def build_path(self, args):
        """
Method creates the path relative to the environments $USER's RDR2
"Settings" folder.

On *nix:
    Expects the absolute path to your steamapps/compatdata folder. If
    omitted path will default relative to this files location.
    $ python RDR2rx.py -path [absolute path]

On Windows this should just know where things are based on your $USER so
the -path flag is irrelevant."""
        if oName == 'posix':
            rdr_dir = self.base_dir
            if '-path' in args:
                try:
                    rdr_dir = args[args.index('-path') + 1]
                    if not exists(rdr_dir):
                        rdr_dir = self.base_dir
                except IndexError as e:
                    sExit(e)

            usr = ['1174180', 'pfx', 'drive_c', 'users', 'steamuser']
            usr = oJoin(rdr_dir, *usr)
        else:
            usr = expanduser('~')

        rdr_loc = ['My Documents', 'Rockstar Games', 'Red Dead Redemption 2',
                  'Settings']

        return oJoin(usr, *rdr_loc)

    def remove(self):
        """
Removes the sga_vulkan_final_init files. """
        if exists(self.rdr2_loc):
            for file_ in listdir(self.rdr2_loc):
                if splitext(file_)[0] == 'sga_vulkan_final_init':
                    SGA_FILE = oJoin(self.rdr2_loc, file_)
                    try:
                        remove(SGA_FILE)
                    except Exception as e:
                        ## Im positive an oserror or permission error
                        ## will be here one day, so 'e' will have to do.
                        sExit(e)
                    else:
                        if not exists(SGA_FILE):
                            print(f'Removed file: {SGA_FILE}')
                        else:
                            print(f'Removed file failure: {SGA_FILE}')
        else:
            ret = "Expected steam proton compatdata folder location.\n"
            ret += f'{self.rdr2_loc}'
            print(ret)


class RedDeadProc():
    """
Class to find and control Red Dead 2 processes. """

    def __init__(self, *arg):
        """
Set up our known processes and find out if they are running. """
        self.Launcher = self.running('Launcher.exe')
        self.RDR2 = self.running('RDR2.exe')
        self.PlayRDR2 = self.running('PlayRDR2.exe')

    def running(self, proc):
        """
Method returns a applications process info or None if the process is not
running. """
        try:
            return [x for x in psIter() if proc == x.name()][0]
        except IndexError:
            return None

    def kill(self, proc):
        """
Method will kill our processes. Expects proc argument to be a
pstuil.process_iter process instance.

You can alternatively pass the '-all' flag as proc to kill all windows
processes running. POSIX only for windows safety."""

        if proc == "-all" and oName == 'posix':
            all_ = [x for x in psIter() if splitext(x.name())[1] == '.exe']
            return bool([bool(x.kill()) for x in all_ if x is not None])

        if proc is not None:
            return bool(proc.kill())

        return False


class RedDead(RedDeadProc, RedDeadSGA):
    """
Class is a controller for its parent classes.  """
    def __init__(self, *arg):
        """
Set up our parents __init__'s and runs user defined processes. """
        RedDeadProc.__init__(self, *arg)
        self.proc_ctrl(arg)
        RedDeadSGA.__init__(self, *arg)
        self.sga_crtl(arg)

    def proc_ctrl(self, args):
        """
Method controls which processes are killed. You can kill:

    1. All windows process. Looks for anything with a .exe and kills it.
       Not recommended on windows.
            -all

    2. Kill Launcher.exe.
            -Launcher

    3. Kill RDR2.exe.
            -RDR2

    4. Kill PlayRDR2.exe.
            -PlayRDR2 """

        if '-kill' in args:
            if '-all' in args:
                self.kill('-all')
            if '-Launcher' in args:
                self.kill(self.Launcher)
            if '-RDR2' in args:
                self.kill(self.RDR2)
            if '-PlayRDR2' in args:
                self.kill(self.PlayRDR2)

    def sga_crtl(self, args):
        """
Method controls the removal of sga_vulcan files.

If the -wait flag is passed this program will make sure that the
Launcher.exe has been running for 10 minutes before removing the
sga_vulkan files. Time can be overridden by passing a positive integer
after the -wait flag. """
        if '-rm-sga' in args:
            if '-wait' in args and self.Launcher is not None:
                delay = 10
                try:
                    tmp = args[args.index('-wait') + 1]
                    if tmp[0] != '-' and isinstance(int(tmp), int):
                        delay = tmp
                    else:
                        print(f'Integer expected, got: {tmp}')
                except (IndexError, ValueError):
                    print(f'Value after -wait must be integer, got: {tmp}')

                wait_time = timedelta(minutes=delay)
                started = datetime.fromtimestamp(self.Launcher.create_time())
                now = datetime.now()

                if now > started + wait_time:
                    self.remove()
                else:
                    wait = (started + wait_time) - now
                    sleep(wait.seconds)
                    self.remove()
            else:
                self.remove()


if __name__ == '__main__':

    if 1 in [1 for x in ['-h', '-help'] if x in argv]:
        ## Let's do "get help!".
        sExit(__doc__)

    RedDead(*[x for x in argv if '=' not in x])
